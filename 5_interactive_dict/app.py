import json
from difflib import get_close_matches

data = json.load(open("data.json"))


def define(word):
    word = word.lower()
    if word in data:
        return data[word]
    elif word.title() in data:
        return data[word.title()]
    elif word.upper() in data:
        return data[word.upper()]
    else:
        close_matches = get_close_matches(word, data.keys())
        if len(close_matches) > 0:
            best_match = close_matches.pop(0)
            yn = input("Did you mean '%s'? Y/N " % best_match)
            if yn.lower() == "y":
                return data[best_match]
            else:
                return "There is no definition for '%s'" % word
        else:
            return "There is no definition for '%s'" % word


w = input("Enter word: ")
output = define(w)
if type(output) == list:
    for i in output:
        print(i)
else:
    print(output)
