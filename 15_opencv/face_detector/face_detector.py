"""Face detector using opencv-python and haarcascades"""
# pylint: disable=C0103
import cv2

def main(photo):
    """Identify faces in an image"""
    face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
    img = cv2.imread(photo)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray_img, scaleFactor=1.1, minNeighbors=5)

    for x, y, w, h in faces:
        img = cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 3)

    cv2.imshow("Image", cv2.resize(img, (int(img.shape[1]/1.5), int(img.shape[0]/1.5))))
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main("photo.jpg")
    main("news.jpg")
