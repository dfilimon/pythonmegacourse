"""Capture video from video camera"""
import cv2

def main():
    """main function"""
    video = cv2.VideoCapture(0)
    i = 0
    while True:
        i = i + 1
        check, frame = video.read()

        cv2.imshow("Capture", frame)
        key = cv2.waitKey(1)

        if key == ord('q'):
            break

    video.release()
    cv2.destroyAllWindows()
    print(i)

if __name__ == '__main__':
    main()
