""" Resize all files in the "images" folder
"""
import os
import cv2

for file in os.listdir("images"):
    img = cv2.imread("images/" + file)
    resized_img = cv2.resize(img, (100, 100))
    cv2.imwrite("images/resized_" + file, resized_img)
