"""
A program that stores book  information:
Title, Author
Year, ISBN

Users can:
View all records
Search
Add an entry
Update
Delete
Close
"""

# pyinstaller --onefile --windowed app.py

from tkinter import *
from backend import Database

database=Database("books.db")

def get_selected_row(event):
	try:
		global selected_row
		index=lb1.curselection()[0]
		selected_row=lb1.get(index)
		e1.delete(0,END)
		e1.insert(END,selected_row[1])
		e2.delete(0,END)
		e2.insert(END,selected_row[2])
		e3.delete(0,END)
		e3.insert(END,selected_row[3])
		e4.delete(0,END)
		e4.insert(END,selected_row[4])
	except IndexError:
		pass

def view_command():
    lb1.delete(0,END)
    for row in database.view():
        lb1.insert(END,row)

def search_command():
    lb1.delete(0,END)
    for row in database.search(title_text.get(),author_text.get(),year_text.get(),isbn_text.get()):
        lb1.insert(END,row)

def add_command():
    database.insert(title_text.get(),author_text.get(),year_text.get(),isbn_text.get())
    lb1.delete(0,END)
    lb1.insert(END,(title_text.get(),author_text.get(),year_text.get(),isbn_text.get()))

def delete_command():
    database.delete(selected_row[0])
    lb1.delete(0,END)
    view_command()

def update_command():
    database.update(selected_row[0],title_text.get(),author_text.get(),year_text.get(),isbn_text.get())
    view_command()

window=Tk()

window.wm_title("BookStore")

l1=Label(window,text="Title")
l1.grid(row=0, column=0)

l1=Label(window,text="Author")
l1.grid(row=0, column=2)

l1=Label(window,text="Year")
l1.grid(row=1, column=0)

l1=Label(window,text="ISBN")
l1.grid(row=1, column=2)

title_text=StringVar()
e1=Entry(window,textvariable=title_text)
e1.grid(row=0,column=1)

author_text=StringVar()
e2=Entry(window,textvariable=author_text)
e2.grid(row=0,column=3)

year_text=StringVar()
e3=Entry(window,textvariable=year_text)
e3.grid(row=1,column=1)

isbn_text=StringVar()
e4=Entry(window,textvariable=isbn_text)
e4.grid(row=1,column=3)

lb1=Listbox(window,height=6,width=35)
lb1.grid(row=2,column=0,columnspan=2,rowspan=6)

sb1=Scrollbar(window)
sb1.grid(row=2,column=2,rowspan=6)

lb1.configure(yscrollcommand=sb1.set)
sb1.configure(command=lb1.yview)

lb1.bind('<<ListboxSelect>>',get_selected_row)

b1=Button(window,text="View all",width=12,command=view_command)
b1.grid(row=2,column=3)

b2=Button(window,text="Search",width=12,command=search_command)
b2.grid(row=3,column=3)

b3=Button(window,text="Add",width=12,command=add_command)
b3.grid(row=4,column=3)

b4=Button(window,text="Update",width=12,command=update_command)
b4.grid(row=5,column=3)

b5=Button(window,text="Delete",width=12,command=delete_command)
b5.grid(row=6,column=3)

b6=Button(window,text="Close",width=12,command=window.destroy)
b6.grid(row=7,column=3)

window.mainloop()
