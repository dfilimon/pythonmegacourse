class Account:
    
    def __init__(self, filepath):
        self.filepath = filepath
        with open(filepath, 'r') as file:
            self.balance = int(file.read())

    def withdraw(self, amount):
        if self.balance > amount:
            self.balance -= amount

    def deposit(self, amount):
        self.balance += amount
    
    def commit(self):
        with open(self.filepath, 'w') as file:
            file.write(str(self.balance))

class CheckingAccount(Account):
    """Extension of Account class with ability to transfer funds"""
    
    type = "checking"

    def __init__(self, filepath, fee):
        Account.__init__(self, filepath)
        self.fee = fee

    def transfer(self, amount):
        self.balance -= amount + self.fee



# account = Account("balance.txt")
# print(account.balance)
# account.withdraw(200)
# print(account.balance)
# account.deposit(300)
# print(account.balance)
# account.commit()

checking = CheckingAccount("balance.txt", 1)
checking.transfer(50)
print(checking.balance)
checking.commit()