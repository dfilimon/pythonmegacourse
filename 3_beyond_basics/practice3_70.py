import glob2
from datetime import datetime


def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f") + ".txt"


def merge_files(files):
    with open(get_timestamp(), 'w') as output:
        for f in files:
            with open(f) as file:
                output.write(file.read() + '\n')


my_files = glob2.glob("pythonmegacourse\\beyond_basics\\*.txt")
merge_files(my_files)
