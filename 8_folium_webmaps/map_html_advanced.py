import folium
import pandas

data = pandas.read_csv("volcanoes.csv")
latitude = list(data["LAT"])
longitude = list(data["LON"])
elevation = list(data["ELEV"])
name = list(data["NAME"])

info = """<h4>Volcano information:</h4>
<a href="https://www.google.com/search?q=%%22%s%%22" target="_blank">%s</a><br>
Height: %s m
"""

map = folium.Map(location=[38.58, -99.09], zoom_start = 5, tiles="Mapbox Bright")
fg = folium.FeatureGroup(name="My Map")

for lat, lon, elev, name in zip(latitude, longitude, elevation, name):
    iframe = folium.IFrame(html=info % (name, name, elev), width=200, height=100)
    fg.add_child(folium.Marker(location=(lat, lon), popup=folium.Popup(iframe), icon=folium.Icon(color='green')))

map.add_child(fg)
map.save("Map_html_advanced.html")
