import folium
import pandas


def colorize(elevation):
    if elevation <= 2000:
        return 'green'
    elif elevation <= 3000:
        return 'orange'
    else:
        return 'red'


data = pandas.read_csv('Volcanoes.txt')
latitude = list(data['LAT'])
longitude = list(data['LON'])
elevation = list(data['ELEV'])

map = folium.Map(location=[38.58, -99.09], zoom_start=5, tiles='Mapbox Bright')

fg_volcanoes = folium.FeatureGroup(name='Volcanoes')

for lat, lon, elev in zip(latitude, longitude, elevation):
    #fg.add_child(folium.Marker(location=[lat, lon], popup=str(elev) + " m", icon=folium.Icon(color=colorize(elev))))
    fg_volcanoes.add_child(folium.CircleMarker(location=[lat, lon], radius=6, popup=str(elev) + ' m', fill_color=colorize(elev), color='grey', fill_opacity=0.7))

fg_population = folium.FeatureGroup(name='Population')
fg_population.add_child(folium.GeoJson(data=open('world.json', 'r', encoding='utf-8-sig').read(),
style_function=lambda x: {'fillColor':'green' if x['properties']['POP2005'] < 10000000
else 'orange' if 10000000 <= x['properties']['POP2005'] < 20000000 else 'red' }))

map.add_child(fg_volcanoes)
map.add_child(fg_population)
map.add_child(folium.LayerControl())

map.save('Map1.html')
